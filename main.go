package main

import (
	"encoding/json"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Hello")

	hello := widget.NewLabel("test")
	w.SetContent(container.NewVBox(
		hello,
		widget.NewButton("hi", func() {
			hello.SetText("u got it")
		}),
	))
	w.ShowAndRun()

	json.Marshal(CreatureStats{
		Name: "Megatherium",
		Health: Stat{
			Base:       740,
			WildGrowth: 148,
			TameGrowth: 5.4,
		},
		Stamina: Stat{
			Base:       400,
			WildGrowth: 40,
			TameGrowth: 10,
		},
		Oxygen: Stat{
			Base:       270,
			WildGrowth: 27,
			TameGrowth: 10,
		},
		Food: Stat{
			Base:       3000,
			WildGrowth: 300,
			TameGrowth: 10,
		},
		Weight: Stat{
			Base:       725,
			WildGrowth: 14.5,
			TameGrowth: 4.5,
		},
		Melee: Stat{
			Base:       32,
			WildGrowth: 1.6,
			TameGrowth: 1.7,
		},
		Speed: Stat{
			Base:       100,
			WildGrowth: 0,
			TameGrowth: 1,
		},
		Torpidity: Stat{
			Base:       1000,
			WildGrowth: 60,
			TameGrowth: 0,
		},
	})
}
