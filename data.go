package main

// Stat
// See https://ark.wiki.gg/wiki/Creature_stats_calculation for an explanation
type Stat struct {
	Base          float64 `json:base`
	WildGrowth    float64 `json:wild+`
	TameGrowth    float64 `json:tame+`
	TameBonusAdd  float64
	TameBonusMult float64
}

// Defined in game settings
type StatModifier struct {
	WildGrowth    float64
	TameGrowth    float64
	TameBonusAdd  float64
	TameBonusMult float64
}

type GlobalStatModifiers struct {
	Health    StatModifier `json:hp`
	Stamina   StatModifier `json:stam`
	Oxygen    StatModifier `json:oxy`
	Food      StatModifier `json:food`
	Weight    StatModifier `json:weight`
	Melee     StatModifier `json:melee`
	Speed     StatModifier `json:speed`
	Torpidity StatModifier `json:torp`
}

type CreatureStats struct {
	Name      string `json:name`
	Health    Stat   `json:hp`
	Stamina   Stat   `json:stam`
	Oxygen    Stat   `json:oxy`
	Food      Stat   `json:food`
	Weight    Stat   `json:weight`
	Melee     Stat   `json:melee`
	Speed     Stat   `json:speed`
	Torpidity Stat   `json:torp`
}
